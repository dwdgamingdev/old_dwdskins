package net.downwithdestruction.skins;

import net.downwithdestruction.skins.commands.*;
import net.downwithdestruction.skins.configuration.MainConfig;
import net.downwithdestruction.skins.listeners.*;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.keyboard.Keyboard;

public class DwDSkins extends JavaPlugin {
	public static DwDSkins instance;
	private final Logger logger = Logger.getInstance();
	private final PluginManager pluginManager = Bukkit.getPluginManager();
	
	public DwDSkins() {
		instance = this;
	}
	
	public static DwDSkins getInstance() {
		return instance;
	}
	
	public void onEnable() {
		// Check for SpoutPlugin
		if (pluginManager.getPlugin("Spout") == null || !pluginManager.isPluginEnabled("Spout")) {
			logger.info("DwDSkins requires SpoutPlugin to run, please download Spout at http://get.spout.org - Shutting down...");
			pluginManager.disablePlugin(this);
			return;
		}
		
		// Register event listener
		logger.debug("Registering event listener");
		pluginManager.registerEvents(new SpoutListener(), this);
		
		// Register the commands
		this.getCommand("skin").setExecutor(new CmdSkin());
		this.getCommand("cape").setExecutor(new CmdCape());
		
		// Register key binding
		String hotKey = new MainConfig().getHotKey();
		logger.debug("Registering key binding on KEY_" + hotKey);
		Keyboard key = Keyboard.valueOf("KEY_" + hotKey);
		if (key == null) {
			key = Keyboard.KEY_F7;
		}
		SpoutManager.getKeyBindingManager().registerBinding("DwDSkins_GUI", key, "Opens the Skins/Capes GUI", new KeyListener(this), this);
		
		// Schedule Task
		Integer skinRefreshRate = new MainConfig().getSkinRefreshRate(); // in seconds
		if (skinRefreshRate == null || skinRefreshRate < 1) {
			skinRefreshRate = 3;
		}
		logger.debug("Starting skin refresh scheduled task. Refresh set to " + skinRefreshRate + " seconds.");
		getServer().getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
			public void run() {
				Utils.loadAllPlayerSkinsCapes();
			}
		}, ((long) skinRefreshRate * 20), (long) skinRefreshRate * 20);
		
		logger.info("DwDSkins v" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		logger.info("DwDSkins Disabled.");
	}
}
