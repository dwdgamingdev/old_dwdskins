package net.downwithdestruction.skins.configuration;

import java.io.File;

import net.downwithdestruction.skins.DwDSkins;

public class MainConfig extends Configuration {
	public MainConfig() {
		super(new File(DwDSkins.getInstance().getDataFolder() + File.separator + "config.yml"));
		load();
	}
	
	@Override
	public void load() {
		super.load();
		this.getConfig().addDefault("gui-hotkey", "F7");
		this.getConfig().addDefault("skin-refresh-rate", 5);
		this.getConfig().addDefault("debug-mode", false);
		this.getConfig().options().copyDefaults(true);
		this.save();
	}
	
	public String getHotKey() {
		return this.getString("gui-hotkey");
	}
	
	public int getSkinRefreshRate() {
		return this.getInteger("skin-refresh-rate");
	}
	
	public boolean debug() {
		return getBoolean("debug-mode");
	}
}
