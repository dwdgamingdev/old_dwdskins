package net.downwithdestruction.skins.configuration;

import java.io.File;
import java.io.IOException;

import net.downwithdestruction.skins.Logger;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public abstract class Configuration {
	private final Logger logger = Logger.getInstance();
	private FileConfiguration fileConfiguration;
	private File file;
	
	public Configuration(File file) {
		this.file = file;
	}
	
	public FileConfiguration getConfig() {
		return fileConfiguration;
	}
	
	public void load() {
		try {
			// Create our file if it doesn't exist
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			fileConfiguration = YamlConfiguration.loadConfiguration(file);
			logger.config("Loaded configuration file at " + file.getAbsolutePath());
		} catch (IOException e) {
			logger.severe("Config file at " + file.getAbsolutePath() + " failed to load: " + e.getMessage());
		}
	}
	
	public void save() {
		try {
			fileConfiguration.save(file);
			logger.config("Saved config file at: " + file.getAbsolutePath());
		} catch (IOException e) {
			logger.severe("Config file at " + file.getAbsolutePath() + " failed to save: " + e.getMessage());
		}
	}
	
	public ConfigurationSection getConfigurationSection(String path) {
		if (fileConfiguration == null) return null;
		return fileConfiguration.getConfigurationSection(path);
	}
	
	public boolean isBoolean(String path) {
		return fileConfiguration.isBoolean(path);
	}
	
	public void set(Object value, String path) {
		if (fileConfiguration == null) return;
		fileConfiguration.set(path, value);
		save();
	}
	
	public void setString(String value, String path) {
		if (fileConfiguration == null) return;
		fileConfiguration.set(path, value);
		save();
	}
	
	public void setInteger(Integer value, String path) {
		if (fileConfiguration == null) return;
		fileConfiguration.set(path, value);
		save();
	}
	
	public void setBoolean(Boolean value, String path) {
		if (fileConfiguration == null) return;
		fileConfiguration.set(path, value);
		save();
	}
	
	public Object get(String path) {
		if (fileConfiguration == null) return null;
		return fileConfiguration.get(path);
	}
	
	public String getString(String path) {
		if (fileConfiguration == null) return null;
		return fileConfiguration.getString(path);
	}
	
	public Integer getInteger(String path) {
		if (fileConfiguration == null) return null;
		return fileConfiguration.getInt(path);
	}
	
	public boolean getBoolean(String path) {
		return fileConfiguration != null && fileConfiguration.getBoolean(path);
	}
}
