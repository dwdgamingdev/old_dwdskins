package net.downwithdestruction.skins.configuration;

import java.io.File;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import net.downwithdestruction.skins.Logger;
import net.downwithdestruction.skins.DwDSkins;

public class PlayerConfig extends Configuration {
	private final Logger logger = Logger.getInstance();
	
	public PlayerConfig(OfflinePlayer player) {
		super(new File(DwDSkins.getInstance().getDataFolder() + File.separator + "userdata" + File.separator + player.getName().toLowerCase() + ".yml"));
		chkUserDir(DwDSkins.getInstance());
		this.load();
	}
	
	public PlayerConfig(Player player) {
		super(new File(DwDSkins.getInstance().getDataFolder() + File.separator + "userdata" + File.separator + player.getName().toLowerCase() + ".yml"));
		chkUserDir(DwDSkins.getInstance());
		this.load();
	}
	
	public PlayerConfig(String player) {
		super(new File(DwDSkins.getInstance().getDataFolder() + File.separator + "userdata" + File.separator + player.toLowerCase() + ".yml"));
		chkUserDir(DwDSkins.getInstance());
		this.load();
	}
	
	private void chkUserDir(DwDSkins plugin) {
		// Make userdata directory if it doesnt exist.
		File dir = new File(plugin.getDataFolder() + File.separator + "userdata" + File.separator);
		if (!dir.exists()) {
			try {
				if (dir.mkdir()) {
					logger.config("Created userdata directory.");
				}
			} catch (Exception e) {
				logger.severe("Failed to make userdata directory!");
				logger.severe(e.getMessage());
			}
		}
	}
	
	public String getSkin() {
		return this.getString("custom-skin");
	}
	
	public String getCape() {
		return this.getString("custom-cape");
	}
	
	public void setSkin(String skin) {
		this.setString(skin, "custom-skin");
	}
	
	public void setCape(String cape) {
		this.setString(cape, "custom-cape");
	}
}
