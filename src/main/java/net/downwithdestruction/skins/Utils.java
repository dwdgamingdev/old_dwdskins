package net.downwithdestruction.skins;

import net.downwithdestruction.skins.configuration.PlayerConfig;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.RemoteConsoleCommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

public class Utils {
	public static boolean isAuthorized(final CommandSender player, final String node) {
		return  player instanceof RemoteConsoleCommandSender ||
				player instanceof ConsoleCommandSender ||
				player.hasPermission("dwdskins.op") ||
				player.hasPermission(node);
	}
	
	public static void sendNotification(SpoutPlayer player, String label, String message, int materialId) {
		Logger.getInstance().debug("Sending notification to " + player.getName() + ": " + label + " - " + message);
		player.sendNotification(label, message, Material.getMaterial(materialId));
	}
	
	public static void loadAllPlayerSkinsCapes() {
		for (Player player : DwDSkins.getInstance().getServer().getOnlinePlayers()) {
			if (((SpoutPlayer) player).isSpoutCraftEnabled()) {
				PlayerConfig pc = new PlayerConfig(player);
				String skin = pc.getSkin();
				String cape = pc.getCape();
				try {
					if (skin != null && !skin.equals("") && !skin.equals("http://")) {
						((SpoutPlayer) player).setSkin(skin);
					}
					if (cape != null && !cape.equals("") && !cape.equals("http://")) {
						((SpoutPlayer) player).setCape(cape);
					}
				} catch(Exception e) {
					// do nothing (no log, dont want to flood console for stupid users)
				}
			}
		}
	}
}
