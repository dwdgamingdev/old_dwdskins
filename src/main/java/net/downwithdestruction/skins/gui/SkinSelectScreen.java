package net.downwithdestruction.skins.gui;

import net.downwithdestruction.skins.DwDSkins;
import net.downwithdestruction.skins.Utils;
import net.downwithdestruction.skins.configuration.PlayerConfig;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.event.screen.ButtonClickEvent;
import org.getspout.spoutapi.gui.Color;
import org.getspout.spoutapi.gui.GenericButton;
import org.getspout.spoutapi.gui.GenericGradient;
import org.getspout.spoutapi.gui.GenericLabel;
import org.getspout.spoutapi.gui.GenericPopup;
import org.getspout.spoutapi.gui.GenericTextField;
import org.getspout.spoutapi.gui.RenderPriority;

public class SkinSelectScreen extends GenericPopup {
	public GenericTextField skinUrlField;
	public GenericTextField capeUrlField;
	
	public SkinSelectScreen(DwDSkins plugin, Player player) {
		// Background used to visualize container
		GenericGradient gradient = new GenericGradient();
		gradient.setTopColor(new Color(0.25F, 0.25F, 0.25F, 0.9F));
		gradient.setBottomColor(new Color(0.35F, 0.35F, 0.35F, 0.9F));
		gradient.setWidth(400).setHeight(168);
		gradient.setX(10).setY(20);
		gradient.setPriority(RenderPriority.Highest);
		
		// Dialog title
		GenericLabel title = new GenericLabel();
		title.setX(15).setY(25);
		title.setPriority(RenderPriority.High);
		title.setWidth(390).setHeight(20);
		title.setText(ChatColor.AQUA + "UserCP" + ChatColor.YELLOW + " - " + ChatColor.BLUE + "Select Custom Skin & Cape");
		
		// Label "Skin URL:"
		GenericLabel skinurllabel = new GenericLabel();
		skinurllabel.setX(15).setY(40);
		skinurllabel.setWidth(390).setHeight(20);
		skinurllabel.setPriority(RenderPriority.High);
		skinurllabel.setText("Skin URL (PNG):");
		
		PlayerConfig pc = new PlayerConfig(player);
		String url = "";
		
		// Skin URL to png text box
		skinUrlField = new GenericTextField();
		skinUrlField.setX(16).setY(50);
		skinUrlField.setWidth(388).setHeight(45);
		skinUrlField.setPriority(RenderPriority.Normal);
		skinUrlField.setMaximumCharacters(300);
		skinUrlField.setMaximumLines(3);
		url = pc.getString("custom-skin");
		if (url == null || url.equals("") || url.equals("http://")) {
			skinUrlField.setText("http://");
		} else {
			skinUrlField.setText(url);
		}
		
		// Label "Cape URL:"
		GenericLabel capeurllabel = new GenericLabel();
		capeurllabel.setX(15).setY(105);
		capeurllabel.setWidth(390).setHeight(20);
		capeurllabel.setPriority(RenderPriority.High);
		capeurllabel.setText("Cape URL (PNG):");
		
		// Cape URL to png text box
		capeUrlField = new GenericTextField();
		capeUrlField.setX(16).setY(115);
		capeUrlField.setWidth(388).setHeight(45);
		capeUrlField.setPriority(RenderPriority.Normal);
		capeUrlField.setMaximumCharacters(300);
		capeUrlField.setMaximumLines(3);
		url = pc.getString("custom-cape");
		if (url == null || url.equals("") || url.equals("http://")) {
			capeUrlField.setText("http://");
		} else {
			capeUrlField.setText(url);
		}
		
		// Save skin button
		GenericButton save = new GenericButton() {
			@Override
			public void onButtonClick(ButtonClickEvent event) {
				String skinurl = skinUrlField.getText();
				String capeurl = capeUrlField.getText();
				boolean changed = false;
				PlayerConfig pc = new PlayerConfig(event.getPlayer());
				if (skinurl != null && !skinurl.equals("") && !skinurl.equals("http://")) {
					pc.setString(skinurl, "custom-skin");
					event.getPlayer().setSkin(skinurl);
					changed = true;
				}
				if (capeurl != null && !capeurl.equals("") && !capeurl.equals("http://")) {
					pc.setString(capeurl, "custom-cape");
					event.getPlayer().setCape(capeurl);
					changed = true;
				}
				if (!changed) {
					Utils.sendNotification(event.getPlayer(), "ERROR!", "Please enter valid URLs", 377);
					return;
				}
				event.getPlayer().getMainScreen().getActivePopup().close();
			}
		};
		save.setX(15).setY(165);
		save.setWidth(50).setHeight(20);
		save.setPriority(RenderPriority.Low);
		save.setText("Save");
		
		// Remove skin button
		GenericButton del = new GenericButton() {
			@Override
			public void onButtonClick(ButtonClickEvent event) {
				PlayerConfig pc = new PlayerConfig(event.getPlayer());
				pc.setString(null, "custom-skin");
				pc.setString(null, "custom-cape");
				event.getPlayer().resetSkin();
				event.getPlayer().resetCape();
				event.getPlayer().getMainScreen().getActivePopup().close();
			}
		};
		del.setX(80).setY(165);
		del.setWidth(50).setHeight(20);
		del.setPriority(RenderPriority.Low);
		del.setText("Defaults");
		
		// Close GUI button
		GenericButton close = new GenericButton() {
			@Override
			public void onButtonClick(ButtonClickEvent event) {
				event.getPlayer().getMainScreen().getActivePopup().close();
			}
		};
		close.setX(355).setY(165);
		close.setWidth(50).setHeight(20);
		close.setPriority(RenderPriority.Low);
		close.setText("Close");
		
		this.setTransparent(false);
		this.attachWidgets(plugin, gradient, title, skinurllabel, skinUrlField, capeurllabel, capeUrlField, save, del, close);
	}
}
