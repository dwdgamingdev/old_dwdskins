package net.downwithdestruction.skins.listeners;

import net.downwithdestruction.skins.Utils;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.getspout.spoutapi.event.spout.SpoutCraftEnableEvent;

public class SpoutListener implements Listener {
	@EventHandler
	public void onSpoutCraftEnableEvent(SpoutCraftEnableEvent event){
		Utils.loadAllPlayerSkinsCapes();
	}
	
	@EventHandler
	public void onPlayerChangedWorldEvent(PlayerChangedWorldEvent event) {
		Utils.loadAllPlayerSkinsCapes();
	}
}
