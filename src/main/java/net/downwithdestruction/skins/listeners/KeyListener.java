package net.downwithdestruction.skins.listeners;

import org.getspout.spoutapi.event.input.KeyBindingEvent;
import org.getspout.spoutapi.gui.ScreenType;
import org.getspout.spoutapi.keyboard.BindingExecutionDelegate;

import net.downwithdestruction.skins.DwDSkins;
import net.downwithdestruction.skins.gui.SkinSelectScreen;

public class KeyListener implements BindingExecutionDelegate {
	DwDSkins plugin;
	
	public KeyListener(DwDSkins plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public void keyPressed(KeyBindingEvent event) {
		if (event.getScreenType() != ScreenType.GAME_SCREEN) {
			return;
		}
		event.getPlayer().getMainScreen().attachPopupScreen(new SkinSelectScreen(plugin, event.getPlayer()));
	}
	
	@Override
	public void keyReleased(KeyBindingEvent event) {
		// Auto-generated method stub
	}
}
