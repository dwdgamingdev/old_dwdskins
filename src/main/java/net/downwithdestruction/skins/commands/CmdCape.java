package net.downwithdestruction.skins.commands;

import net.downwithdestruction.skins.DwDSkins;
import net.downwithdestruction.skins.Utils;
import net.downwithdestruction.skins.configuration.PlayerConfig;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

public class CmdCape implements CommandExecutor {
	public CmdCape() {
		//
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("cape")) {
			if (!Utils.isAuthorized(cs, "dwdskins.cape")) {
				cs.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
				return true;
			}
			SpoutPlayer player = ((SpoutPlayer) cs);
			if (!player.isSpoutCraftEnabled()) {
				cs.sendMessage(ChatColor.RED + "This command is only available to Spoutcraft users.");
				return true;
			}
			if (args.length < 1) {
				return false;
			} else if (args[0].equals("default")) {
				if (args.length > 1) {
					if (!Utils.isAuthorized(cs, "dwdskins.others")) {
						cs.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
						return true;
					}
					Player onlinePlayer = DwDSkins.getInstance().getServer().getPlayer(args[1].trim());
					if (onlinePlayer == null) {
						cs.sendMessage(ChatColor.RED + "That player does not exist!");
						return true;
					}
					PlayerConfig pc = new PlayerConfig(onlinePlayer);
					pc.setString(null, "custom-cape");
					((SpoutPlayer) onlinePlayer).resetCape();
					cs.sendMessage(ChatColor.BLUE + args[1] + "'s cape has been reset to default.");
					onlinePlayer.sendMessage(ChatColor.RED + "An admin has reset your cape to default.");
				} else {
					PlayerConfig pc = new PlayerConfig(player);
					pc.setString(null, "custom-cape");
					player.resetCape();
					cs.sendMessage(ChatColor.BLUE + "Cape reset to default.");
				}
			} else {
				if (args[0].contains("?")) {
					cs.sendMessage(ChatColor.RED + "ERROR: Please refrain from using url's with question marks in them. Operation aborted.");
					return true;
				}
				try {
					PlayerConfig pc = new PlayerConfig(player);
					pc.setString(args[0], "custom-cape");
					player.setCape(args[0]);
				} catch (Exception e) {
					// do nothing (no log, dont want to flood console because of stupid users)
				}
				cs.sendMessage(ChatColor.BLUE + "Cape set.");
			}
			return true;
		}
		return false;
	}
}
