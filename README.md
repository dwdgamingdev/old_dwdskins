DwDSkins
========

Bukkit/Spoutplugin plugin adding skins and capes for Spoutcraft users.

commands:
    cape:
        description: Allows you to set your cape.
        usage: /<command> [default/url]
    skin:
        description: Allows you to set your skin.
        usage: /<command> [default/url]

permissions:
    dwdskins.cape:
        description: Allows you to set your own cape texture.
        default: op
    dwdskins.skin:
        description: Allows you to set your own skin texture.
        default: op
    dwdskins.others:
        description: Allows you to reset other players' skin and cape textures.
        default: op
